---
title: dot
weight: 1
description: >
  [hierarchical or layered drawings](https://en.wikipedia.org/wiki/Layered_graph_drawing)
  of directed graphs.
---

`dot` is the default tool to use if edges have directionality.

The layout algorithm aims edges in the same direction (top to bottom, or left
to right) and then attempts to avoid edge crossings and reduce edge length.

- [PDF Manual](/pdf/dot.1.pdf)
- [User Guide](/pdf/dotguide.pdf) (caveat: not current with latest features of Graphviz)
- [Browse code](https://gitlab.com/graphviz/graphviz/-/tree/main/lib/dotgen)

<p style="text-align: center;">
  <a href="/Gallery/directed/cluster.html">
    <img src="/Gallery/directed/cluster.svg">
  </a>
</p>

