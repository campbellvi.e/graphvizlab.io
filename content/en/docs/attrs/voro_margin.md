---
defaults:
- '0.05'
flags:
- notdot
minimums:
- '0.0'
title: voro_margin
types:
- double
used_by: G
description: Factor to scale up drawing to allow margin for expansion i
---
Voronoi technique. `dim' = (1+2*margin)*dim`.
